<?php

namespace Scio\core\app\web;

use Scio\di\configuration\ScioDIConfigurator;
use Yii;
use yii\web\Application;

class ScioApplication extends Application{
	public function __construct( $config = array() ){
		parent::__construct( $config );
		
		$diConfigurer = new ScioDIConfigurator();
		
		$diConfigurer->configureYiiDI( Yii::$container );
	}
	
	public static function getContainer(){
		return Yii::$container;
	}
	
	public static function getLanguageID(){
		// 1 stands for pl_PL
		return 1;
	}
}
