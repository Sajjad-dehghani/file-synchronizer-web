<?php

namespace Scio\core\autoloader;


final class ScioAutoloader{
	const AUTOLOADER_AVAILABLE_PREFIXES = [ 'Scio' ];
	
	public static $instance;
	
	
	public function __construct(){
		$this->initAutoloader();
	}
	
	public function initAutoloader(){
		spl_autoload_register( $this->getAutoloadFunction() );
	}
	
	private function isClassLoadable( $className ){
		$chunks = explode( '\\', $className );
		
		$prefix = $chunks[ 0 ];
		
		return in_array( $prefix, static::AUTOLOADER_AVAILABLE_PREFIXES );
	}
	
	private function handleClassLoad( $className ){
		return include_once '..' . DIRECTORY_SEPARATOR . str_replace( '\\', '/', $className ) . '.php';
	}
	
	public function getAutoloadFunction(){
		return function( $className ){
			if( $this->isClassLoadable( $className ) ){
				return $this->handleClassLoad( $className );
			}
			
			return FALSE;
		};
	}
}

ScioAutoloader::$instance = new ScioAutoloader();
