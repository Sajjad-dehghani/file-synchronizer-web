<?php

namespace Scio\core\db;

use Yii;
use yii\base\Object;
use yii\db\Connection;


class SCIODBManager extends Object{
	private $connection;
	
	
	public function __construct( $config = array() ){
		parent::__construct( $config );
		
		$this->connection = new Connection( Yii::$app->getDb() );
	}
	
	public function sqlQuery( $sql, $parameters ){
		$command = $this->connection->createCommand( $sql, $parameters );
		
		return $command->queryAll();
	}
	
	/**
	 * @param string $sql
	 * @param array $parameters
	 * @return boolean
	 */
	public function sqlQueryExists( $sql, $parameters ){
		$sql = "SELECT EXISTS( $sql ) as exist;";
		
		$result = $this->sqlQuery( $sql, $parameters );
		
		if( isset( $result[ 0 ] ) ){
			return $result[ 0 ][ 'exist' ];
		}
		
		return FALSE;
	}
	
	/**
	 * @return Connection
	 */
	public function getConnection(){
		return $this->connection;
	}
}
