<?php

namespace Scio\di\configuration;

use Scio\core\db\SCIODBManager;
use Scio\synchronizer\FileSynchronizationHandler;
use Scio\synchronizer\service\ScioFileSynchronizationService;
use Scio\synchronizer\session\ScioSessionService;
use Scio\synchronizer\session\SOAPSessionProvider;
use Yii;
use yii\di\Container;


class ScioDIConfigurator{
	private function getDIConfigration(){
		$configuration = [
		
		];

		return $configuration;
	}

	public function configureYiiDI( Container $container ){
		$diConfiguration = $this->getDIConfigration();
		
		foreach( $diConfiguration as $src => $target ){
			$container->set( $src, $target );
		}
		
		$container->set( FileSynchronizationHandler::class, ScioFileSynchronizationService::class, [ Yii::$app->params[ 'WS.FileSync.SaveURL' ] ] );
		$container->setSingleton( SCIODBManager::class, SCIODBManager::class );
		$container->set( SOAPSessionProvider::class, ScioSessionService::class, [  Yii::$app->params[ 'WS.Session.Login'],  Yii::$app->params[ 'WS.Session.Password'],  Yii::$app->params[ 'WS.Session.HashAlgo'] ] );
	}

}
