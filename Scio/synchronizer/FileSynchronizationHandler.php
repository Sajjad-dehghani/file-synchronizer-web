<?php

namespace Scio\synchronizer;

use app\models\synchronizer\dto\FileParam;


interface FileSynchronizationHandler{
	public function handleSoapFile( FileParam $fileParam );
}
