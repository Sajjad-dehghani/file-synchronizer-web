<?php

namespace Scio\synchronizer\service;

use app\models\ar\FileCl;
use app\models\ar\FileClName;
use app\models\synchronizer\dto\FileParam;
use Scio\core\app\web\ScioApplication;
use Scio\core\db\SCIODBManager;
use Scio\synchronizer\FileSynchronizationHandler;
use Yii;
use yii\base\Object;
use yii\db\Exception;


class ScioFileSynchronizationService extends Object implements FileSynchronizationHandler{
	private $pathToFileSave;
	private $scioDBManager;

	
	public function __construct( $pathToFileSave, SCIODBManager $scioDBManager, $config = array() ){
		$this->pathToFileSave = $pathToFileSave;
		$this->scioDBManager = $scioDBManager;

		parent::__construct( $config );
	}

	public function handleSoapFile( FileParam $fileParam ){
		$fileParam->data = base64_decode( $fileParam->data );

		$fileDir = $this->getFileDir( $fileParam );

		if( !is_dir( $fileDir ) ){
			$result = mkdir( $fileDir, 0775, true );

			if( !$result ){
				throw new Exception( "Failed to create directories: {$fileDir}" );
			}
		}
		
		$filePath = $fileDir . DIRECTORY_SEPARATOR . $fileParam->fileName;
		
		if( file_exists( $filePath ) ){
			unlink( $filePath );
		}
		
		file_put_contents( $fileDir . DIRECTORY_SEPARATOR . $fileParam->fileName, $fileParam->data );

		$this->createOrUpdateFileRecord( $fileParam );
	}

	private function getFileDir( FileParam $fileParam ){
		return $this->pathToFileSave . DIRECTORY_SEPARATOR . $fileParam->filePath;
	}

	public function createOrUpdateFileRecord( FileParam $fileParam ){
		unset( $fileParam->data );
		
		$this->scioDBManager->getConnection()->transaction( function() use( $fileParam ) {
			$recordAID = "AID_{$fileParam->filePath}/{$fileParam->fileName}";
			
			$fileCLRecord = FileCl::find()->where( [ 'aid' => $recordAID ] )->one();
			if( $fileCLRecord === NULL ){
				$fileCLRecord = new FileCl();
			}

			$fileCLRecord->aid = $recordAID;
			$fileCLRecord->path = $fileParam->filePath === '' || $fileParam->filePath === NULL ? '/' : $fileParam->filePath;
			$fileCLRecord->is_private = TRUE;
			$fileCLRecord->save();
			
			$fileCLNameRecord = FileClName::find()->where( [ 'cl_id' => $fileCLRecord->id, 'language_id' => ScioApplication::getLanguageID() ] )->one();
			if( $fileCLNameRecord === NULL ){
				$fileCLNameRecord = new FileClName();
			}
			
			$fileCLNameRecord->language_id = ScioApplication::getLanguageID();
			$fileCLNameRecord->cl_id = $fileCLRecord->id;
			$fileCLNameRecord->name = $fileParam->fileName;
			$fileCLNameRecord->save();
		} );
	}

}
