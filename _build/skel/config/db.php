<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=scio_file_synchronizer',
    'username' => 'USER',
    'password' => 'PASS',
    'charset' => 'utf8',
];
