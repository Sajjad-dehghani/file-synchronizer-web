<?php

namespace app\controllers;

use app\models\synchronizer\dto\FileParam;
use app\models\synchronizer\dto\SOAPResult;
use conquer\services\WebServiceAction;
use Scio\synchronizer\FileSynchronizationHandler;
use Scio\synchronizer\session\SOAPSessionProvider;
use yii\db\Exception;
use yii\web\Controller;


class SoapController extends Controller{
	private $fileSynchronizationHandler;
	private $soapSessionProvider;
	
	
	public function __construct( $id, $module, FileSynchronizationHandler $fileSyncHandler, SOAPSessionProvider $soapSessionProvider , $config = array() ){
		$this->fileSynchronizationHandler = $fileSyncHandler;
		$this->soapSessionProvider = $soapSessionProvider;
		
		parent::__construct( $id, $module, $config );
	}
	
	public function actions(){
		return [
			'synchronizer' => [
				'class' => WebServiceAction::class,
				'classMap' => [ 'FileParam' => 'app\models\synchronizer\dto\FileParam' ]
				],
		];
	}
	
	private function handleResponseOnException( SOAPResult $soapResult, Exception $ex ){
		$soapResult->errorCode = 1;
		$soapResult->errorMessage = \yii\helpers\VarDumper::export( $ex );
		$soapResult->value = NULL;
	}
	
	/**
	 * @param string $sessionID
	 * @param app\models\synchronizer\dto\FileParam $fileParam
	 * @return app\models\synchronizer\dto\SOAPResult
	 * @soap
	 */
	public function synchronizeFile( $sessionID, FileParam $fileParam ){
		$soapResult = new SOAPResult();
		
		try{
			if( !$this->soapSessionProvider->getIsSessionValid( $sessionID ) ){
				throw new Exception( 'invalid session' );
			}
			
			$this->fileSynchronizationHandler->handleSoapFile( $fileParam );
			
			$soapResult->errorCode = 0;
		}catch( Exception $ex ){
			$this->handleResponseOnException( $soapResult, $ex );
		}
		
		return $soapResult;
	}
	
	/**
	 * @param string $login
	 * @param string $password
	 * @return app\models\synchronizer\dto\SOAPResult containing sessionID in value
	 * @soap
	 */
	public function createSession( $login, $password ){
		$soapResult = new SOAPResult();
		
		try{
			$soapResult->errorCode = 0;
			$soapResult->value = $this->soapSessionProvider->authenticate( $login, $password );
		} catch (Exception $ex) {
			$this->handleResponseOnException( $soapResult, $ex );
		}
		
		return $soapResult;
	}
}
