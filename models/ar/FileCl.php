<?php

namespace app\models\ar;

use Yii;

/**
 * This is the model class for table "file_cl".
 *
 * @property integer $id
 * @property string $aid
 * @property string $path
 * @property integer $is_private
 *
 * @property FileClName[] $fileClNames
 */
class FileCl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file_cl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['aid', 'path'], 'required'],
            [['is_private'], 'integer'],
            [['aid'], 'string', 'max' => 128],
            [['path'], 'string', 'max' => 512],
            [['aid'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'aid' => 'Aid',
            'path' => 'Path',
            'is_private' => 'Is Private',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFileClNames()
    {
        return $this->hasMany(FileClName::className(), ['cl_id' => 'id']);
    }
}
