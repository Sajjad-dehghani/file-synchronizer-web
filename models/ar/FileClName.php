<?php

namespace app\models\ar;

use Yii;

/**
 * This is the model class for table "file_cl_name".
 *
 * @property integer $id
 * @property integer $cl_id
 * @property string $name
 * @property string $description
 * @property integer $language_id
 *
 * @property FileCl $cl
 * @property Language $language
 */
class FileClName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file_cl_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cl_id', 'name', 'language_id'], 'required'],
            [['cl_id', 'language_id'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 512],
            [['cl_id'], 'exist', 'skipOnError' => true, 'targetClass' => FileCl::className(), 'targetAttribute' => ['cl_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cl_id' => 'Cl ID',
            'name' => 'Name',
            'description' => 'Description',
            'language_id' => 'Language ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCl()
    {
        return $this->hasOne(FileCl::className(), ['id' => 'cl_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
}
