<?php

namespace app\models\ar;

use Yii;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $aid
 *
 * @property FileClName[] $fileClNames
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['aid'], 'required'],
            [['aid'], 'string', 'max' => 45],
            [['aid'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'aid' => 'Aid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFileClNames()
    {
        return $this->hasMany(FileClName::className(), ['language_id' => 'id']);
    }
}
