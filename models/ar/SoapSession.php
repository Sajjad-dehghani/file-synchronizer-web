<?php

namespace app\models\ar;

use Yii;

/**
 * This is the model class for table "soap_session".
 *
 * @property integer $id
 * @property string $creation_date_and_time
 * @property string $aid
 */
class SoapSession extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'soap_session';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creation_date_and_time', 'aid'], 'required'],
            [['creation_date_and_time'], 'safe'],
            [['aid'], 'string', 'max' => 128],
            [['aid'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creation_date_and_time' => 'Creation Date And Time',
            'aid' => 'Aid',
        ];
    }

    /**
     * @inheritdoc
     * @return SoapSessionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SoapSessionQuery(get_called_class());
    }
}
