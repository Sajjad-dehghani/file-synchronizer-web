<?php

namespace app\models\synchronizer\dto;


class FileParam{
	/**
	 * @var string
	 * @soap
	 */
	public $data;
	
	/**
	 * @var string
	 * @soap 
	 */
	public $fileName;
	
	/**
	 * @var string
	 * @soap
	 */
	public $filePath;
}
