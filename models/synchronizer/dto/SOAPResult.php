<?php

namespace app\models\synchronizer\dto;


class SOAPResult{
	/**
	 * @var int
	 * @soap
	 */
	public $errorCode;
	
	/**
	 * @var string
	 * @soap
	 */
	public $errorMessage;
	
	/**
	 * @var string
	 * @soap 
	 */
	public $value;
}
